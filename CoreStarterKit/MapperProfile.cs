﻿using AutoMapper;
using CoreStarterKit.Models;
using CoreStarterKit.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CoreStarterKit
{
    public class MapperProfile
        : Profile
    {
        public MapperProfile()
        {
            CreateMap<ProductViewModel, Product>();
            CreateMap<Product, ProductViewModel>();
        }
    }
}
