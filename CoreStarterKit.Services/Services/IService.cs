﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;

namespace CoreStarterKit.Services
{
    public interface IService<TModel>
    {
        TModel Add(TModel model);

        TModel Update(TModel model);

        void Remove(TModel model);
        void Remove(long Id);

        TModel Get(TModel model);
        TModel Get(long Id);

        IEnumerable<TModel> List();
        IEnumerable<TModel> List(Expression<Func<TModel, bool>> predicate);
    }
}
