﻿using System;

namespace CoreStarterKit.Models
{
    public class Product
        : BaseModel
    {
        public string ProductCode { get; set; }
        public string Name { get; set; }
        public string Detail { get; set; }

        public decimal Price { get; set; }

    }
}
