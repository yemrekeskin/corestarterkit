﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CoreStarterKit.Models
{
    public enum ActivePassive
    {
        Active = 0,
        Passive = 1
    }
}
