﻿using CoreStarterKit.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace CoreStarterKit.Services
{
    public interface IProductRepository
       : IRepository<Product>
    {

    }
}
