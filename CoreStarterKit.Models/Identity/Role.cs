﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace CoreStarterKit.Models
{
    public class Role
        : IdentityRole<long>
    {
        public string RoleName { get; set; }
        public string Explanation { get; set; }
    }
}
