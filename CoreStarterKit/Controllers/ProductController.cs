﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CoreStarterKit.Models;
using CoreStarterKit.Services;
using Microsoft.AspNetCore.Mvc;

namespace CoreStarterKit.Controllers
{
    public class ProductController
        : BaseController
    {
        private readonly IProductService productService;

        public ProductController(IProductService productService)
        {
            this.productService = productService;
        }

        public IActionResult Index()
        {
            var inserted = productService.Add(new Product()
            {
                ProductCode = "NE-123",
                Name = "Computer",
                Detail = "Lorem Inpuls",
                Price = 123
            });

            var list = productService.List();

            inserted.ProductCode = "RM-123";
            productService.Update(inserted);

            var list1 = productService.List();

            productService.Remove(inserted.Id);

            var list2 = productService.List();

            return View();
        }
    }
}