﻿using CoreStarterKit.Models;
using Microsoft.EntityFrameworkCore;

namespace CoreStarterKit.Services
{
    public class ApplicationDbContext
        : DbContext, IDbContext
    {
        // Business Models
        public DbSet<Product> Products { get; set; }

        // Identity Models
        public DbSet<User> Users { get; set; }
        public DbSet<Role> Roles { get; set; }
        public DbSet<RoleClaim> RoleClaims { get; set; }
        public DbSet<UserClaim> UserClaimns { get; set; }
        public DbSet<UserLogin> UserLogins { get; set; }
        public DbSet<UserRole> UserRoles { get; set; }
        public DbSet<UserToken> UserTokens { get; set; }

        public ApplicationDbContext()
        {
        }

        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer("Server=(localdb)\\mssqllocaldb;Database=CoreStarterKit_Db;Trusted_Connection=True;MultipleActiveResultSets=true");
            base.OnConfiguring(optionsBuilder);
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            // Business Model Mapping
            var appSchema = "core";

            modelBuilder.Entity<Product>(model =>
            {
                model.ToTable("Products", appSchema);
            });

            // Identity Model Mapping
            var IdentitySchema = "auth";

            modelBuilder.Entity<User>(entity =>
            {
                entity.ToTable("Users", IdentitySchema);
            });
            modelBuilder.Entity<UserLogin>(entity =>
            {
                entity.HasKey(d => new { d.LoginProvider, d.ProviderKey, d.UserId });
                entity.ToTable("UserLogins", IdentitySchema);
            });
            modelBuilder.Entity<UserRole>(entity =>
            {
                entity.HasKey(d => new { d.UserId, d.RoleId });
                entity.ToTable("UserRoles", IdentitySchema);
            });
            modelBuilder.Entity<UserClaim>(entity =>
            {
                entity.ToTable("UserClaims", IdentitySchema);
            });
            modelBuilder.Entity<UserToken>(entity =>
            {
                entity.HasKey(d => new { d.LoginProvider, d.UserId });
                entity.ToTable("UserTokens", IdentitySchema);
            });
            modelBuilder.Entity<Role>(entity =>
            {
                entity.ToTable("Roles", IdentitySchema);
            });
            modelBuilder.Entity<RoleClaim>(entity =>
            {
                entity.ToTable("RoleClaims", IdentitySchema);
            });

            base.OnModelCreating(modelBuilder);
        }

        public override int SaveChanges()
        {
            return base.SaveChanges();
        }
    }
}