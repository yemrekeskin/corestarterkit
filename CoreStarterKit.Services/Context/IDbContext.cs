﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace CoreStarterKit.Services
{
    public interface IDbContext
    {
        DbSet<TModel> Set<TModel>() where TModel : class;

        int SaveChanges();
    }
}
