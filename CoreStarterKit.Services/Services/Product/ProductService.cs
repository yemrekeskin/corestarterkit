﻿using CoreStarterKit.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace CoreStarterKit.Services
{

    public class ProductService
        : BaseService<Product>, IProductService
    {
        public ProductService(IProductRepository repo)
            : base(repo)
        {

        }
    }
}
