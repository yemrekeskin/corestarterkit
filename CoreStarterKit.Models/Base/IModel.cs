﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CoreStarterKit.Models
{
    public interface IModel
        : IModel<int>
    {

    }

    public interface IModel<T>
    {
        T Id { get; set; }
    }
}
