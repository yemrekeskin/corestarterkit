﻿using CoreStarterKit.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace CoreStarterKit.Services
{
    public class ProductRepository
        : BaseRepository<Product>, IProductRepository
    {

    }
}
